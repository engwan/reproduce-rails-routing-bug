Rails.application.routes.draw do
  get '*namespace_id/:project_id/bar', to: 'root#index', constraints: { namespace_id: /(?!api\/)[a-zA-Z0-9_\\]+/, project_id: /[a-zA-Z0-9]+/ }

  # This version works:
  #get '*namespace_id/:project_id/bar', to: 'root#index', constraints: lambda { |req| req.params[:namespace_id].match?(/\A(?!api\/)[a-zA-Z0-9_\\]+\z/) && req.params[:project_id].match?(/\A[a-zA-Z0-9]+\z/) }

  get '/api/foo/bar', to: 'root#api'
end
