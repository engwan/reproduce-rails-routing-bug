# README

To reproduce the bug, visit `/api/foo/bar/`. You will get an exception like so:

```
Started GET "/api/foo/bar/" for 127.0.0.1 at 2023-02-03 14:04:19 +0800

NoMethodError (undefined method `names' for nil:NilClass):

actionpack (7.0.4.2) lib/action_dispatch/journey/router.rb:126:in `block in find_routes'
actionpack (7.0.4.2) lib/action_dispatch/journey/router.rb:123:in `map!'
actionpack (7.0.4.2) lib/action_dispatch/journey/router.rb:123:in `find_routes'
actionpack (7.0.4.2) lib/action_dispatch/journey/router.rb:32:in `serve'
actionpack (7.0.4.2) lib/action_dispatch/routing/route_set.rb:852:in `call'
rack (2.2.6.2) lib/rack/tempfile_reaper.rb:15:in `call'
rack (2.2.6.2) lib/rack/etag.rb:27:in `call'
rack (2.2.6.2) lib/rack/conditional_get.rb:27:in `call'
rack (2.2.6.2) lib/rack/head.rb:12:in `call'
actionpack (7.0.4.2) lib/action_dispatch/http/permissions_policy.rb:38:in `call'
actionpack (7.0.4.2) lib/action_dispatch/http/content_security_policy.rb:36:in `call'
rack (2.2.6.2) lib/rack/session/abstract/id.rb:266:in `context'
rack (2.2.6.2) lib/rack/session/abstract/id.rb:260:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/cookies.rb:704:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/callbacks.rb:27:in `block in call'
activesupport (7.0.4.2) lib/active_support/callbacks.rb:99:in `run_callbacks'
actionpack (7.0.4.2) lib/action_dispatch/middleware/callbacks.rb:26:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/executor.rb:14:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/actionable_exceptions.rb:17:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/debug_exceptions.rb:28:in `call'
web-console (4.2.0) lib/web_console/middleware.rb:132:in `call_app'
web-console (4.2.0) lib/web_console/middleware.rb:28:in `block in call'
web-console (4.2.0) lib/web_console/middleware.rb:17:in `catch'
web-console (4.2.0) lib/web_console/middleware.rb:17:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/show_exceptions.rb:26:in `call'
railties (7.0.4.2) lib/rails/rack/logger.rb:40:in `call_app'
railties (7.0.4.2) lib/rails/rack/logger.rb:25:in `block in call'
activesupport (7.0.4.2) lib/active_support/tagged_logging.rb:99:in `block in tagged'
activesupport (7.0.4.2) lib/active_support/tagged_logging.rb:37:in `tagged'
activesupport (7.0.4.2) lib/active_support/tagged_logging.rb:99:in `tagged'
railties (7.0.4.2) lib/rails/rack/logger.rb:25:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/remote_ip.rb:93:in `call'
actionpack (7.0.4.2) lib/action_dispatch/middleware/request_id.rb:26:in `call'
rack (2.2.6.2) lib/rack/method_override.rb:24:in `call'
rack (2.2.6.2) lib/rack/runtime.rb:22:in `call'
```
